const AWS = require('aws-sdk')
const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'})

exports.handler = (event, context, callback) => {
	const searchTrack = (trackData) => {
		const { id, playlistId } = trackData

        const params = {
            Key: {
                "playlistId": {
                    S: playlistId
                }
            }, 
            TableName: "playlistItems"
		}
		
		dynamodb.getItem(params, (err, data) => {
            if (err) {
                console.log('Error. Getting item(s) from dynamodb failed: ', JSON.stringify(err, null, 2))
                callback(err, null)
            }
            else {
                try {
					const tracks = JSON.parse(data.Item.tracks.S)
					const track = tracks.find(track => track.id === id)
                    callback(null, track)
                }
                catch(e) {                   
                    callback(e, null)
                }
            }
        })
	}

	searchTrack(event.body)
}